package it.alessiorossato.multiclient.repository;

import it.alessiorossato.multiclient.domain.Auto;
import org.aspectj.apache.bcel.util.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AutoDAO  extends JpaRepository<Auto, String>
{

}
