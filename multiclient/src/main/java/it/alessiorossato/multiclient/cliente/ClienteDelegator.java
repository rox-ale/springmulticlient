package it.alessiorossato.multiclient.cliente;

import org.springframework.stereotype.Component;

@Component
public interface ClienteDelegator {

    public void start();
}
