package it.alessiorossato.multiclient.cliente;

import it.alessiorossato.multiclient.repository.AutoDAO;
import org.springframework.beans.factory.annotation.Autowired;

public class ClienteB extends ClienteDefault {
@Autowired
	AutoDAO autoDAO;

	@Override
	public void start() {
		System.out.println(autoDAO.findAll().toString());

		System.out.println("cliente b");
	}
}
