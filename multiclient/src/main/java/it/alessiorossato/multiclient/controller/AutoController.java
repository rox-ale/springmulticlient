package it.alessiorossato.multiclient.controller;

import it.alessiorossato.multiclient.cliente.ClienteDelegator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/auto")
//@CrossOrigin(origins = "*", maxAge = 3600)
//@CrossOrigin(origins="http://localhost:4200")
public class AutoController {

	@Autowired
	ClienteDelegator clienteDelegator;



	@GetMapping(value = "/test")
	public ResponseEntity<?> listArtByEan() throws ClassNotFoundException {

		// qui stama il variable clienteA e clienteB per vedere il valore
clienteDelegator.start();
		return new ResponseEntity<>("articolo", HttpStatus.OK);

	}

}
