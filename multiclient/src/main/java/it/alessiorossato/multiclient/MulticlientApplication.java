package it.alessiorossato.multiclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MulticlientApplication {

	public static void main(String[] args) {
		SpringApplication.run(MulticlientApplication.class, args);
	}

}
