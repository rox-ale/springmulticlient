package it.alessiorossato.multiclient.clienteConfig;

import it.alessiorossato.multiclient.cliente.ClienteA;
import it.alessiorossato.multiclient.cliente.ClienteB;
import it.alessiorossato.multiclient.cliente.ClienteDefault;
import it.alessiorossato.multiclient.cliente.ClienteDelegator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class ClienteDelegatorConfiguration {

    @Bean
    @ConditionalOnProperty(value="tipo.cliente", havingValue="default", matchIfMissing=true)
    ClienteDelegator clienteDefault() {
        return new ClienteDefault();
    }

    @Bean
    @ConditionalOnProperty(value="tipo.cliente", havingValue="clienteA", matchIfMissing=false)
    ClienteDelegator clienteA() {
        

        return new ClienteA();
    }

    @Bean
    @ConditionalOnProperty(value="tipo.cliente", havingValue="clienteB", matchIfMissing=false)
    ClienteDelegator clienteB() {
        return new ClienteB();
    }
}