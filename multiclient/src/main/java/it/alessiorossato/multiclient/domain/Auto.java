package it.alessiorossato.multiclient.domain;




import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "auto")

public class Auto  implements Serializable
{
    private static final long serialVersionUID = 291353626011036772L;

    @Id
    @Column(name = "targa")

    private String targa;

    @Column(name = "colore")
    private String colore;

    @Column(name = "porte")
    private int porte;

    public Auto() {
    }

    public String getTarga() {
        return targa;
    }

    public void setTarga(String targa) {
        this.targa = targa;
    }

    public String getColore() {
        return colore;
    }

    public void setColore(String colore) {
        this.colore = colore;
    }

    public int getPorte() {
        return porte;
    }

    public void setPorte(int porte) {
        this.porte = porte;
    }

    @Override
    public String toString() {
        return "Auto{" +
                "targa='" + targa + '\'' +
                ", colore='" + colore + '\'' +
                ", porte=" + porte +
                '}';
    }
}
